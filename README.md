### YAMH ###
Yet Another Metropolis-Hastings. An R package trying to take the best of MHadaptive and a private repository to efficiently sample models with the Metropolis-Hastings algorithm. 

It aims to add: 

* auto-stopping : using Geweke and Raftery Lewis it stops as soon as enough sampling has been done
* handling of numerical (instable) likelihoods, such as the synthetic likelihood
* handling of parameters defined on intervals

The core function is MCMC(), see the help of this function to get into the package. The criteria for stopping a single chain can be investigated looking at the help of CombinedDiag. 

### Test it ###
Download/clone, the help is yamh.pdf in the main folder. Launch R in the yamh folder then: 

    library(devtools)
    document()
    ?MCMC
And do the example.

Also check 
    ?GetMonitored
And
    ?Traces

For visualization of the results

MetroHastingsAutoStop() is lightly documented and in development but much faster than MCMC().
### Install it ###
To install it, launch R in the yamh folder then:

    library(devtools)
    document()
    install()
    
### Using the synthetic likelihood for spatial data? ###
Try out our [repository with spatial statistics and dispersal models](https://bitbucket.org/cbarbu/synlikspatial)