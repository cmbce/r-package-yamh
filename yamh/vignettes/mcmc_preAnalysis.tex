\documentclass{article}
\usepackage[ascii]{inputenc}
\usepackage[LGR,T1]{fontenc}
\usepackage[greek,english]{babel}
\usepackage{amsmath}
\usepackage{amssymb,amsfonts,textcomp}
\usepackage{dsfont}
\usepackage{array}
\usepackage{hhline}
\usepackage{graphicx}
\usepackage{algorithm2e}% in ubuntu package texlive-science
\providecommand{\e}[1]{\ensuremath{\times 10^{#1}}}
\bibliographystyle{plos2009}
% \usepackage{supertabular}
\usepackage{booktabs}
\usepackage{changepage}
\title{
  How to begin with synlik: quick evaluation of spatial statistics and model fitting using the synthetic likelihood
}

\author{C.M.Barbu, K. Seth, M.Z. Levy}

%% for knitr
%% for inline R code: if the inline code is not correctly parsed, you will see a message
\newcommand{\rinline}[1]{SOMETHING WRONG WITH knitr}
\newcommand{\code}[1]{\texttt{#1}}
%% begin.rcode setup, include=FALSE
% library("knitr")
% opts_chunk$set(fig.path='figure/latex-', cache.path='cache/latex-',echo=FALSE)
%% end.rcode

\date{}

\usepackage[figurename=Figure,tablename=Table]{caption}
% \renewcommand{\thetable}{S1.\arabic{table}}
% \renewcommand{\thefigure}{S1.\arabic{figure}}

%vertically centered multi line cell default horizontally centered but l/r are accepted
\newcommand{\mlc}[2][c]	
{
\begin{tabular}[c]{@{}#1@{}} #2 \end{tabular}
}
\begin{document}
\maketitle
\tableofcontents

  We provide a framework to quickly estimate the informativeness and coverage quality of statistics and groups of statistics for a given set of parameters $\theta_0$.
  This quick evaluation of the statistics is done in two times. First individual functions returning statistics (\code{statfn}) are evaluated, allowing to identify good and possibly complementary statistics using \code{slStatsEval}. Second, reusing the individual evaluation, evaluations of groups of \code{statfn}s can be done using \code{slCombStats}. Hereafter we describe in detail how these functions are implemented and justify the approximation they use. 

  The chosen statistics can then be used to run a full estimation of the parameters using a largely automated MCMC chain. 

  Finally we advise to verify the estimation by running two type of verifications: 
  \begin{itemize}
    \item model/stats compatibility
    \item model/data compatibility
  \end{itemize}

  We additionally have some fast approximation tools that allow much faster evaluation of the quality of the parameters.
\section{Evaluation of ``individual'' statistics}
  Our framework allow the simultaneous evaluation and comparison of multiple ``individual'' statistics. We use individual between quotes here as an ``individual'' statistic may be a set of statitics of a same type, for example the values of the semi-variance at different distance classes or the temporal autocorrelation at different lags.

  The computational gain is obtained by allowing multiple statistics to be evaluated on a common set of simulations from the parameter set of interest $\theta_0$. A typical marginal probability distribution for each parameter $k$ at $\theta_0$ as well as the corresponding expected coverage are evaluated in two steps. First, multiple pairs ($\theta,l_s(\theta_0|y_{\theta_0})$) are calculated for multiple $y_{\theta_0}$. Second, these pairs are analysed to give expected precision and coverage.

  \subsection{Generation of multiple pairs ($\theta,l_s(\theta|y_{\theta_0})$ for multiple $y_{\theta_0}$}
    First multiple $y_{\theta_0}$ are simulated from $\theta_0$. Then the synthetic likelihood of these simulated data $y_{\theta_0}$ are evaluated for multiple $\theta$. We distinguish three possibilities for the sampling of these $\theta$.
    \subsubsection{Grid sampling}
      Possible for one or two parameters, it is what we use here but should not be considered above three parameters.
    \subsubsection{Sampling from the prior}
      This can be a good solution if the prior is good enough, the user is on his own though to use \code{slMargCre} and \code{slMargCoverage}
    \subsubsection{Automated sampling via MCMC}
      The sampling of relevant $\theta$ can be automated via MCMC on a relevant simulated data sample $y_{\theta_0}^r$. Along the MCMC, all statistics are estimated and the likelihood of multiple $y_{\theta_0}$ is assessed but only the most relevant statistics are employed to guide the MCMC. The chain can optionally be heated to better explore relatively low likelihoods according to $y_{\theta_0}^r$.

      This can be done with a simple call of \code{slMCMC}:
      If \code{statsfn} is a vector of functions, the the likelihood are generated for all of the stats, the first one is used to guide the sampler. 
      In a similar way if \code{simulstats} is a vector of functions the first will be used to generate $\Theta$ the sets of parameter values but the likelihood profile and coverage estimates will be given for each of the modelstat function.

      In addition, all the $s_\theta$ are saved to allow quick secondary evaluation of new combination of statistics

      \paragraph{Automated choice of the reference $y_{\theta_0}$ for the MCMC}
      We sample around a ``central'' dataset: the one with highest likelihood.
      \begin{itemize}
	\item draw a set $Y_{\theta_0}$ of data from $\theta_0$
	\item get ($\mu_{\theta_0}$,$\Sigma_{\theta_0}$)
	\item choose the $y_{\theta_0}$ with the highest $l_s\left(y_{\theta_0}|\mu_{\theta_0},\Sigma_{\theta_0}\right)$
      \end{itemize}
  \subsection{Estimation of the likelihood profiles CI and coverage from multiple triplets ($\theta,y_{\theta_0},l_s(\theta|y_{\theta_0})$)}
    \subsubsection{Likelihood profile and CS/CI}
      Once obtained for each $y_{\theta_0}$ a large number of pairs ($\theta,y_{\theta_0},l_s(\theta|y_{\theta_0})$), marginal profiles can be plotted, smoothed and used to define credible intervals.
      \paragraph{Likelihood profiles}
      The likelihood profile simply corresponds to the marginal plot of the obtained $l_s(\theta|y_{\theta_0})$ against $\theta$. To facilitate the reading of the profile we add a spline smoothing to this profile using \ldots giving the marginal distribution for $\theta^i$ is the parameter $i$ from $\theta$, later named $sm_{i}(\theta)$.
      \paragraph{Estimation of $tr_{\alpha}^{y_{\theta_0}}$} The marginal smoothed likelihood profiles $sm_{i}(\theta)$ allow to identify the marginal likelihood thresholds $tr_{i,\alpha}^{y_{\theta_0}}$:
      \[tr_{\alpha}^{y_{\theta_0}}: \int_{tr_{\alpha}^{y_{\theta_0}}}^{max\left(l(\theta | y_{\theta_0})\right)} \lambda d\lambda = 1-\alpha \]
      This is done for each parameter $i$ by regularly sampling $r$ along the smoothed likelihood profile between the minimum and maximum values of $\theta^i$, identifying the $1-\alpha$ quantile of the corresponding $sm_i{r}$.

      In a similar way, credible intervals are computed leaving $\frac{1-\alpha}{2}$ of the integral on each side of the $1-\alpha$\% credible interval.

      The \code{slMargCre(params,likelihoods,probs=0.95,plot=FALSE)} function returns a list of arrays. Each array corresponding to a column in \code{params}. Each array line correspond to a different $1-\alpha$ value in \code{probs} and in columns successively the corresponding $tr_{\alpha}$, the left and right boundary of the credible interval (assuming all the likelihoods are derived for a same $y$).

      \paragraph{Identification of the coverage}
      For each parameter $\theta^i$ and each simulation $y_{\theta_0}$ we can then observe if $\theta_0^i$ is included in the Credible Space (CS) and the credible interval (CI).
      The proportion of $y_{\theta_0}$ leading to CS/CI including $\theta_0^i$ indicate the marginal coverage for the parameter $i$.


      The core function is \code{slMargCoverage(refParam,y0,params,liks,probs=0.95)}
      where \code{refParam} is the vector $\theta_0$, \code{y0} indicate the identifier $k$ of the corresponding $y_{\theta_0}^k$ in the \code{params} table which contains the parameters for  \code{liks} the likelihoods $l(\theta|y_{\theta_0}^k)$. Multiple CI/CS size can be specified in probs, a vector of $1-\alpha$ values.

      The coverage for each parameter in \code{refParam} is calculated after identification of the marginal distributions using \code{slMargCre} for each $y_{\theta_0}^k$.

    \subsubsection{Coverage estimation}
      The coverage is simply estimated by measuring for each $y_{\theta_0}$  $\sum_\theta l_s(\theta_0|y_{\theta_0})> tr_{\alpha}^{\theta}l_s(\theta|y_{\theta_0})$
      % \begin{algorithm}[H]
      %   $Y_{\theta_0}$: $n$ realization $y_{\theta_0} \sim Model(\theta_0)$\\
      %   $\Theta$: ($\theta_0$,$n$ sets of $\theta$ drawn from uniform)\\
      %   \For{$\theta$ in $\Theta$}{
      %     estimate $\left( \hat{\mu}_\theta,\hat{\Sigma}_\theta \right)$
      %   }
      %   \For{$y_{\theta_0}$ in $Y_{\theta_0}$}{
      %     \For{$\theta$ in $\Theta$}{
      %       $ll[\theta] \leftarrow p\left(y_{\theta_0}|\hat{\mu}_\theta,\hat{\Sigma}_\theta\right)$
      %     }
      %     dec $\leftarrow$ order[ll[-$\theta_0$]] ; decll $\leftarrow$ ll[dec]\\ 
      %     cumSum $\leftarrow$ cumsum(decll) \\
      %     $tr_{95\%,y_{\theta_0}}\leftarrow$ decll[which(CumSum$>$0.95*cumSum[n])]
      %     $Ind[y_{\theta_0}] \leftarrow ll[\theta_0] > tr_{95\%,y_{\theta_0}}$
      %   }
      %   $C_{95\%,\theta_0} \leftarrow sum(Ind)/n$
      % \end{algorithm}

  \subsection{Covariance estimation}
    The synthetic likelihood framework should account for moderate correlation between your statistics, nevertheless, we have observed that very strong correlation may impair the efficiency of the statistics. To avoid combining very correlated statistics we provide the \code{slPreCor} function that assess and nicely display the covariance and partial-correlation matrices of your statistics at a given $\theta_0$.

\section{Evaluation of combination of statistics}
  Once the efficiency of isolated stats has been evaluated, the combination of these statistics can be quickly evaluated.

  \code{slcombineStats} receive a table of parameters and stats as generated by \code{slMCMC}. This table has in its attributes the names of the statistics generating functions and the corresponding columns in the table. The \code{stats} parameter is a vector of \code{statsFunction} names for which $(\mu_\theta,\Sigma_\theta)$ are estimated for all $\theta$ sampled by \code{slMCMC} then providing corresponding likelihood profile and coverage as previously described.

\section{Parameters estimation}
  Once the adequate statistics are chosen, the desired statistics should be aggregated in one function \code{slMCMC} should be called for this function, without heating to provide marginal and joint parameter estimation.

  Our slMCMC function provides automated features for the MCMC runs: auto-adjustement of the sampling variance, identification of the burnin, convergence, adequate thinning and sufficient sampling (by default to sample a 95\% cI) using first the Raftery and then the Geweke diagnostics. It also saves all parameter sets tested as well as corresponding statistics and $l_s$ values in a text file to allow the continuation of interupted chains. A listener is also set to allow "smooth" interuption of the chain.

\section{Post-analysis}

  \subsection{Model/stats adequation}
    The tools provided for the pre-analysis can be used to do a post-estimation analysis of the coverage: \code{slMCMC} should be run for the estimated median or mean of the parameter set. The \code{slPostCov} functions provides a wrapper for that.

  \subsection{Model/data adequation}
    If the former analysis guarantees the quality of the estimation, the estimated values only reflect the best possible fit of the model to the data. We strongly recommend to observe the quality of the fit given by the model by comparing statistics on data simulated from the joint posterior to statistics on the real data.

    In this effect we provide \code{slPostCov(params,stats,data,Nobs=NULL)} where \code{params} is a table with in columns the different parameters and in lines the different sets that should be tested. $n$ simulations will be performed for each parameter set so that the total number of simulations is the smaller multiple above 1000 or \code{Nobs} if not NULL.
    If \code{params} is a vector or a one line vector, all simulations are drawn from this vector of parameter.

    \code{slPostCov} returns for each statistics function in \code{stats} a graph with boxplots representing at each statistic the distribution of simulated statistics. The read data statistics are represented by crosses linked by a solid line if their are the statistics function returns multiple stats.

    We recommand to run \code{slPostCov} both on statistics used to fit the model and other statistics to validate the correct description of the used statistics through \code{slMCMC} and validate the reproduction of other important features of the data. 

    In addition we provide \code{slPostCovFull} which actually runs multiple MCMCs on simulated data from $\theta_0$, as this analysis easily takes days to reach any significative estimation (need >600 repetitions) this is mainly used by the developpers of this package on multiple core machines.

\section{Additional and faster tools}
  Given that the initial MCMC estimation may be a bit long we also have developped quicker though not formally well sustained tool to get a good idea of the comparated efficiency of different statistics. 

  A good idea of the impact of the deviation from the MCMC on the coverage can be get from to be \code{slQuickCoverage} (currently \code{local.coverage}, based on a table of summary statistics computed from $\theta_0$ it returns expected coverage with the MVN or the skewed MVN. It's extremely fast and it works relatively well (as compared to full blown analysis with 100 MCMCs on simulated data) and I'm personally convinced this is justified if we assume some ``regularity'' of the statistics (stability of the relationship between $l_s(\theta)$ and the density of the statistics generated by $\theta$ in the neighborhood of $\theta$ but I have troubles defining this regularity in a usefull way.

  A good idea of the comparative precision of statistics and in a quite faster way can be get by assuming $l_s(\theta|\theta_0) \approx l_s(\theta_0|\theta)$ again this is not formally justified but empirical checks show that the estimates are fairly good. In a stats comparison framework this approximation should be justifiable as the deviation from the real is comparable for different statistics. 

\section{General formating guidelines}
  \subsection{Tables of repeted statistics or data}
    We use repetitions on lines and stats or data on columns. This is meant to ease the display of the tables. 
  \subsection{Synthetic likelihood objects: slObj}
    We use a simple list format (see model\_basic.R)
    
  \end{document}
