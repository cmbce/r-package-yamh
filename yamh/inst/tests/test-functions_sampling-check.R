context("test-functions_sampling-check.R")
# library(testthat)
# source("functions_sampling.R")
# source("functions_sampling.R",chdir=TRUE)
# graphics.off()
source("basic_model.R")
test_that("MeanSdToBeta OK",{
    a<-9
    b<-1
    musigback<-BetaToMeanSd(a,b)
    expect_equal(0.9,musigback$mu)

    mu<-0.1
    sig<-0.05
    out<-MeanSdToBeta(mu,sig)
    musigback<-BetaToMeanSd(out$a,out$b)
    expect_equal(mu,musigback$mu)
    expect_equal(sig,musigback$sig)
})

test_that("OmniSample OK for norm/lnorm",{
    # simple model to test OmniSample
    Model<-function(theta,Data,...){
      names(theta)<-Data$parmNames
      LL<-sum(dnorm(Data$y,mean=theta["mean"],sd=theta["sd"],log=TRUE))
      llh<-LL
      names(llh)<-"ll"
      yhat<-rnorm(length(Data$y),mean=theta["mean"],sd=exp(theta["sd"]))
      return(list(llh=llh,
      Dev=-2*LL, # deviance, probably not to be changed
      monitor=c(llh,theta), # to be monitored/ploted
      yhat=yhat, # data generated for that set of parameter
      # will be used for posterior check
      parm=theta # the parameters, possibly constrained by the model
      ))
    }

    # simple Data to test normSample
    nbit<-1000
    upFreq<- 0 # display state every upFreq, 0 to never display
    set.seed(777)
    MyData<-list()
    MyData$parmNames<-c("mean","sd")
    MyData$sampling<-c("norm","lnorm")
    MyData$monNames<-c("llh",MyData$parmNames)
    MyData$realMean<-0 # to be guessed
    MyData$realSd<-1   # to be guessed
    MyData$y<-rnorm(100,mean=MyData$realMean,sd=MyData$realSd)

    # basic initialization
    theta<-c(10,20)

    #--- Necessary Machinery---#
    # init of Data:
    names(MyData$sampling)<-MyData$parmNames
    nparams<-length(theta)

    # init of theta attributes and saving scheme
    outModel<-Model(theta,MyData)
    attributes(theta)$outModel<-outModel
    monitor<-mat.or.vec(nbit+1,length(MyData$monNames))
    monitor[1,]<-outModel$monitor

    accepts<-as.data.frame(matrix(rep(0,nparams*nbit),ncol=nparams))
    names(accepts)<-MyData$parmNames

    # simple MCMC chain
    # Rprof()
    for(numit in 1:nbit){
        if(numit%%upFreq==0 && upFreq!=0 ){
            cat("it:",numit,"of",nbit,"current theta:",theta,"\n");
        }
        theta<-OmniSample(Model,MyData,theta,c(0.4,0.4),repeatSample=FALSE)
        monitor[numit+1,]<-attributes(theta)$monitor
    }
    # Rprof(NULL)

    # post treatment
    monitor<-as.data.frame(monitor)
    names(monitor)<-MyData$monNames
    burn.in<-ceiling(nbit/10)
    estMean<-mean(monitor[-(1:burn.in),"mean"])
    estSd<-mean(monitor[-(1:burn.in),"sd"])
    yMean<-mean(MyData$y)
    ySd<-sd(MyData$y)

    # cat("rateAccept:",apply(accepts,2,mean),"\n")
    # cat("estimate(mean)",estMean,"estimate(sd)",estSd,"\n")

    # dev.new()
    # par(mfrow=c(1,3))
    # plot(monitor[,"llh"])
    # plot(monitor[,"mean"])
    # plot(monitor[,"sd"])

    # dev.new()
    # par(mfrow=c(1,2))
    # hist(monitor[,"mean"])
    # abline(v=estMean,col="black")
    # abline(v=MyData$realMean,col="blue")
    # abline(v=yMean,col="red")
    # hist(monitor[,"sd"])
    # abline(v=estSd,col="black")
    # abline(v=MyData$realSd,col="blue")
    # abline(v=ySd,col="red")

    expect_true(abs(estMean-yMean)<0.1)
    expect_true(abs(estSd-ySd)<0.1)
})

test_that("OmniSample OK for [0, 1] sampling with boundednorm",{
    # guess a binomial rate
    Model<-function(theta,Data){
      names(theta)<-Data$parmNames
      LL<-sum(dbinom(Data$draws,Data$nByDraw,theta[["rate"]],log=TRUE))
      llh<-LL
      yhat<-0
      return(list(llh=llh,
      Dev=-2*LL, # deviance, probably not to be changed
      monitor=c(llh,theta), # to be monitored/ploted
      yhat=yhat, # data generated for that set of parameter
      # will be used for posterior check
      parm=theta # the parameters, possibly constrained by the model
      ))
    }

    # simple Data to test normSample
    nbit<-1000
    upFreq<- 0 # display state every upFreq, 0 to never display
    set.seed(777)
    MyData<-list()
    MyData$parmNames<-c("rate")
    MyData$sampling<-c("boundednorm")
    MyData$monNames<-c("llh",MyData$parmNames)
    MyData$realProba<-0 # to be guessed
    MyData$nByDraw<-100
    MyData$draws<-rbinom(100,MyData$nByDraw,MyData$realProba)

    # basic initialization
    theta<-c(0.5)

    #--- Necessary Machinery---#
    # init of Data:
    names(MyData$sampling)<-MyData$parmNames
    nparams<-length(theta)

    # init of theta attributes and saving scheme
    outModel<-Model(theta,MyData)
    monitor<-mat.or.vec(nbit+1,length(MyData$monNames))
    monitor[1,]<-outModel$monitor
    attributes(theta)$outModel<-outModel

    accepts<-as.data.frame(matrix(rep(0,nparams*nbit),ncol=nparams))
    names(accepts)<-MyData$parmNames

    # simple MCMC chain
    # Rprof()
    for(numit in 1:nbit){
      if(numit%%upFreq==0 && upFreq!=0 ){
        cat("it:",numit,"of",nbit,"current theta:",theta,"\n");
      }
      theta<-OmniSample(Model,MyData,theta,c(0.4,0.4))
      monitor[numit+1,]<-attributes(theta)$monitor
    }
    # Rprof(NULL)

    # post treatment
    monitor<-as.data.frame(monitor)
    names(monitor)<-MyData$monNames
    burn.in<-ceiling(nbit/10)
    estProba<-mean(monitor[-(1:burn.in),"rate"])

    # cat("estimated rate",estProba,"\n")

    # dev.new()
    # par(mfrow=c(1,2))
    # plot(monitor[,"llh"], pch = ".")
    # plot(monitor[,"rate"], pch = ".")

    expect_true(abs(estProba-MyData$realProba)<0.1)
})

#======================================
# standard data and model used in the following tests
#======================================
set.seed(1000)
# make fake data
nbDrawsInData <- 10
fakeData<-rnorm(nbDrawsInData,10,2)
# format the options for the chain
myData <- list(
	       initValues=c(10,2),
	       parmNames=c("mean","sd"),
	       sampling=c("norm","lnorm"),
	       monNames=c("mean","sd","lik"),
	       data=fakeData
	       )
# the model to get the likelihood from the data
ModelS<-function(theta,myData,...){
  dataSimul<-BasicNSimulator(50,theta,10)
  statsSimul<-BasicNStatsMoments(dataSimul,NULL)
  statsData<-BasicNStatsMoments(myData$data,NULL)
  llh<-synLik(sY=t(statsSimul),sy=t(statsData))
  return(list(llh=llh,monitor=c(theta,llh)))
}
# the model to get the likelihood from the data
ModelA<-function(theta,myData,...){
  llh<-sum(dnorm(myData$data,mean=theta[1],sd=theta[2],log=TRUE))
  monitor=c(theta,llh)
  # cat("monitor:",monitor,"\n")
  # cat("myData$monNames:",myData$monNames)
  names(monitor)<-myData$monNames
  return(list(llh=llh,monitor=monitor))
}

test_that("OmniSample with monitorBoth works",{
	  theta<-c(10,2)
nParams <- length(theta)
set.seed(1000)
newTheta<-OmniSample(ModelA,myData,theta,c(0.4,0.4))
set.seed(1000)
newThetaBoth<-OmniSample(ModelA,myData,theta,c(0.4,0.4),monitorBoth=TRUE)
expect_equal(length(attributes(newTheta)$monitor)*2*nParams,
	     length(attributes(newThetaBoth)$monitor))
attributes(newTheta)<-NULL
attributes(newThetaBoth)<-NULL
expect_equal(newTheta,newThetaBoth)
})

test_that("MCMC with monitorBoth works",{
set.seed(1000)
monitored<-MCMC(ModelA,myData,useAutoStop=FALSE,upFreq=0)
set.seed(1000)
monitoredB<-MCMC(ModelA,myData,monitorBoth=TRUE,useAutoStop=FALSE,upFreq=0)
expect_equal(dim(monitoredB)[2],12)
m<-monitored
attributes(m)<-NULL
mb<-monitoredB[,7:9]
attributes(mb)<-NULL
expect_equal(m,mb)
expect_equal(colnames(monitoredB),rep(myData$monNames,2*2))
})

test_that("MCMC respects nMaxSimul",{
	  # check stopping before adaptSD ok
set.seed(1000)
monitored<-MCMC(ModelA,myData,useAutoStop=TRUE,upFreq=0,nMaxSimul=200)
expect_equal(dim(monitored)[1],201)
	  # check stopping after adaptSD ok but before full convergence
set.seed(1000)
monitored<-MCMC(ModelA,myData,useAutoStop=TRUE,upFreq=0,nMaxSimul=2000)
expect_equal(dim(monitored)[1],2001)
	  # check stopping at convergence
set.seed(1000)
monitored<-MCMC(ModelA,myData,useAutoStop=TRUE,upFreq=0,nMaxSimul=20000)
expect_true(dim(monitored)[1]<5000 & dim(monitored)[1]>1000,
	    info=paste("n=",dim(monitored)[1]))
})

test_that("MCMC basic direct likelihood",{
          set.seed(1000)
          # run the MCMC
          system.time(monDirect<-MCMC(ModelA,myData,upFreq=0,repeatSample=FALSE))
          # check:
          # oneDimPrecI(monDirect[,1],monDirect[,3],plot=TRUE) # TODO: fix error
          out<-reweight.marginal.distributions(monDirect[,1],exp(monDirect[,3]))
          plot(out$param,out$lik)
          # TODO: add expect_that()
          # TODO: avoid production of additional files when testing
          expect_length(out,3)
          expect_equal(names(monDirect),c("mean","sd","lik"))

})

test_that("MCMC nMaxSimul ok",{
          set.seed(1000)
          # run the MCMC
          system.time(monDirect<-MCMC(ModelA,myData,upFreq=0,repeatSample=FALSE,nMaxSimul=1000))
          # check:
          # oneDimPrecI(monDirect[,1],monDirect[,3],plot=TRUE) # TODO: fix error
          out<-reweight.marginal.distributions(monDirect[,1],exp(monDirect[,3]))
          plot(out$param,out$lik)
          # TODO: add expect_that()
          # TODO: avoid production of additional files when testing
          expect_length(out,3)
          expect_equal(names(monDirect),c("mean","sd","lik"))
          expect_equal(dim(monDirect),c(1001,3))

})
test_that("MCMC basic synlik",{
          set.seed(1000)
          # make fake data
          nbDrawsInData <- 10
          fakeData<-rnorm(nbDrawsInData,10,2)
          # format the options for the chain
          myData <- list(
                initValues=c(10,2),
                parmNames=c("mean","sd"),
                sampling=c("norm","lnorm"),
                monNames=c("mean","sd","lik"),
                data=fakeData
                )
          # the model to get the likelihood from the data
          Model<-function(theta,myData,...){
            dataSimul<-BasicNSimulator(50,theta,10)
            statsSimul<-BasicNStatsMoments(dataSimul,NULL)
            statsData<-BasicNStatsMoments(myData$data,NULL)
            llh<-synLik(sY=t(statsSimul),sy=t(statsData))
            return(list(llh=llh,monitor=c(theta,llh)))
          }
          # test of the Model:
          out<-Model(myData$initValues,myData)
          # cat("likelihood=",out$llh)
          monitored<-MCMC(Model,myData,nMaxSimul=1000,upFreq=0)
          expect_true(all(monitored$sd>0))
          expect_gt(mean(monitored$mean),8)
          expect_lt(mean(monitored$mean),12)

          # # # run the MCMC
          # # 
          # # save(monitoredS,file="monitored_Basic.img")
          # data(monitoredS)
          # # check:
          # oneDimPrecI(monitoredS[,1],monitoredS[,3],plot=TRUE)
          # out<-reweight.marginal.distributions(monitoredS[,1],exp(monitoredS[,3]))
          # lines(out$param,out$lik)
          # # Oups, tails on the plot are very different
          # # TODO: fix the difference in plots
          # # TODO: add expect_that()
})

test_that("ProbaMeanBinomInInt basically fine",{
expect_true(abs(ProbaMeanBinomInInt(200,1000,c(0.15,0.4))-1)<0.0001)
expect_true(abs(ProbaMeanBinomInInt(0,1000,c(0.15,0.4)))<0.0001)
expect_true(abs(ProbaMeanBinomInInt(500,1000,c(0.15,0.4)))<0.0001)
})

test_that("Default monitoring ok",{
set.seed(777)
Model <- function(theta,...){sum(dnorm(theta,mean=0,sd=1,log=TRUE))}
out <- MCMC(Model,myData=list(initValues=rep(0,5)),upFreq=0)
mon <- GetMonitored(quiet=TRUE)
sum1 <- summary(out,verb=FALSE)
expect_true(is.finite(sum1$mleQuant50))
expect_warning(sum2 <- summary(mon,verb=FALSE))
expect_true(is.finite(sum2$AIC))
expect_true(all(abs(sum1$quant - sum2$quant)/sum1$quant<10e-01))

set.seed(777)
Model <- function(theta,...){list(llh=sum(dnorm(theta,mean=0,sd=1,log=TRUE)))}
out <- MCMC(Model,myData=list(initValues=rep(0,5)),upFreq=0)
sum3 <- summary(out,verb=FALSE)
expect_true(all(abs(sum1$quant - sum3$quant)/sum1$quant<10e-09))
})


