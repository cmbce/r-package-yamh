# context("MHadaptiveAutoStop.R")
# ## Define a Bayesian linear regression model
# test_that("MetroHastingsAutoStop ok",{
# li_reg<-function(pars,data)
# {
#     a<-pars[1]      #intercept
#     b<-pars[2]      #slope
#     sd_e<-pars[3]   #error (residuals)
#     if(sd_e<=0){return(NaN)}
#     pred <- a + b * data[,1]
#     log_likelihood<-sum( dnorm(data[,2],pred,sd_e, log=TRUE) )
#     prior<- prior_reg(pars)
#     return(log_likelihood + prior)
# }
# 
# ## Define the Prior distributions
# prior_reg<-function(pars)
# {
#     a<-pars[1]          #intercept
#     b<-pars[2]          #slope  
#     epsilon<-pars[3]    #error
# 
#     prior_a<-dnorm(a,0,100,log=TRUE)     ## non-informative (flat) priors on all 
#     prior_b<-dnorm(b,0,100,log=TRUE)     ## parameters.  
#     prior_epsilon<-dgamma(epsilon,1,1/100,log=TRUE)      
# 
#     return(prior_a + prior_b + prior_epsilon)
# }                
# 
# # simulate data
# set.seed(777)
# x<-runif(30,5,15)
# y<-x+rnorm(30,0,5)
# d<-cbind(x,y)
# 
# mcmc_ras<-MetroHastingsAutoStop(li_func=li_reg,pars=c(0,1,1),
#                               par_names=c('a','b','epsilon'),data=d,quiet=TRUE,monitor_file=NULL)
# cd <- CombinedDiag(mcmc_ras$trace,logFile=NULL)
# expect_equal(cd$ok,1)
# 
# })
# 
# # mcmc_ri<-Metro_Hastings(li_func=li_reg,pars=c(0,1,1),
# #                        par_names=c('a','b','epsilon'),data=d)
# # mcmc_rasnoadapt<-MetroHastingsAutoStop(li_func=li_reg,pars=c(0,1,1),
# #                               par_names=c('a','b','epsilon'),data=d,checkAutoStop=FALSE)
# # 
# # par(mfrow=c(1,2))
# # accepts <- apply(diff(cbind(mcmc_rasnoadapt$trace,mcmc_rasnoadapt$trace)),1,sum)>0
# # plot(rollapply(accepts,width=100,by=100,FUN=mean,align="left"),main="rasnoadapt")
# # accepts <- apply(diff(cbind(mcmc_ri$trace,mcmc_ri$trace)),1,sum)>0
# # plot(rollapply(accepts,width=100,by=100,FUN=mean,align="left"),main="ri")
# # 
# # #=> better acceptation rate for rasnoadapt than ri
# # 
# # cb_ri<- CombinedDiag(mcmc_ri$trace)
# # cb_rasnoadapt<- CombinedDiag(mcmc_rasnoadapt$trace)
# # #=> rasnoadapt compares honorably with cb_ri but the autocorrelation of the chain seems higher
# # #   it might be worth it to wait a bit more before stopping autocorrelation
# # #   it would be interesting to see how the proposal correlation matrix varies
# # 
