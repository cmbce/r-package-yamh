# CMBC 
# GPL 
# functions to evaluate the results of the sampling

#' read samples in a file
#' @description Equivalent to \code{read.table(myfile,header=TRUE)} but for some reason is much faster
#' @param file The file where to find the samples (in columns, tab delimited and with headers on first line).
#' @param quiet Should the number of items read be printed (and potentially other indications)
#' @return A data frame with the values
#' @examples
#' x <- rnorm(1000)
#' y <- rnorm(1000)
#' samples <- as.data.frame(cbind(x,y))
#' write.table(samples,file="samples.txt",row.names=FALSE,sep="\t")
#' samplesBack <- GetMonitored("samples.txt")
#' expect_equal(samples,samplesBack)
#' @export GetMonitored
GetMonitored <- function(file="thetasamples_all.txt",quiet=FALSE){
    noms<-read.table(file,nrow=1,header=TRUE)
    sampled<-as.data.frame(matrix(scan(skip=1,file=file,sep="\t",quiet=quiet),ncol=dim(noms)[2],byrow=TRUE))
    names(sampled)<-names(noms)
    attributes(sampled)$class <- c("yamhMonitor","data.frame")
    return(sampled)
}
#' plot traces of a data frame
#' @description plot values on the colums of a data.frame 
#' @param db the data frame whose columns are to be plot as traces
#'        if db is not a data.frame it will attempt to coerce it.
#' @param nl number of lines of graphs in the device
#' @param nc number of columns of graphs in the device
#' @param true.vals a vector of values, one for each column of db, that should
#'        be plot as an horizental line on the plot
#' @return nothing, used for it's side effect of plotting the traces
#' @examples 
#' a <- rnorm(10000)
#' b <- rnorm(10000,1,3)
#' traces(cbind(a,b),nl=1,nc=2,true.vals=c(0,1))
#' @export Traces
Traces<-function(db=NULL,nl=3,nc=4,true.vals=NULL){
    if(is.null(db)){db <- GetMonitored()}
  db<-as.data.frame(db)
  if(is.null(names(true.vals)) && length(true.vals) == dim(db)[2]){
	  names(true.vals) <- colnames(db)
  }

  pch <- "."
  if(dim(db)[1]>100){
    type="p"
  }else{
    type="l"
  }
  for(num in 1:length(names(db))){
	  # cat("plotting:",names(db)[num],"\n")
	  # cleanup variable from NaN
	  var <- db[[num]]
	  var <- var[is.finite(var)]
	  if(length(var)==0){
	    cat("No finite values\n")
	    next 
	  }

	  if(num %% (nl*nc) ==1){ 
		  dev.new()
		  par(mfrow=c(nl,nc))
	  }
	  name<-names(db)[num]
	  if(name %in% names(true.vals)){
		  ylim<-range(true.vals[[name]],var)
	  }else{
		  ylim<-range(var)
	  }
	  plot(var,main=name,pch=pch,type=type,ylim=ylim)
	  if(name %in% names(true.vals)){
		  abline(h=true.vals[[name]],col="green")
	  }
  }
}

#' @title Estimates from values
#' @description 
#'        Compute the mean, median and credible interval for the variable
#'        Also get the densities corresponding to the frequence of values in a vector
#'        based on \code{locfit()} in the locfit package.
#' @param C the vector of values for the variable
#' @param name possibly the name of the variable in C
#' @param visu plot the fitted density
#' @param leg  add a legend to the plot of the density
#' @param true.val specific value to be signaled on the plot by a vertical line
#' @param xlim limits for the plot of the density
#' @param visuQuant visualize the credible interval based on 2.5 and 97.5 quantiles and median
#' @param add add the plot the density on the top of current plot
#' @param ... additional values to be passed to plot()
#' @examples
#' x <- rnorm(10000)
#' GetEstimate(x)
#' @export GetEstimate
GetEstimate <- function(C,name="",visu=TRUE,leg=TRUE,true.val=NULL,xlim = NULL,visuQuant=TRUE,add=FALSE,...){
	C<-C[which(is.finite(C))]

  if(length(which(!is.na(C)))>1){
    estimate<-c(mean(C),quantile(C,probs=c(0.025,0.5,0.975)))
    names(estimate)[1]<-"Mean"
    names(estimate)[3]<-"Median"
  }else{
    estimate<-rep(NA,4)
  }
  if(length(levels(as.factor(C)))>1){ # avoid to estimate unvarying
    # fit the density of C, avoiding errors with overspread C 
    Nthin<-1
    subset <- 1:length(C)
    p<-0.001
    require("locfit")
    while(class(densfit<-try(locfit(~lp(C[subset])),silent=TRUE))=="try-error" 
	  && length(subset)>0.90*length(C)){
	    # quantile based 
	    warning(paste("locfit diverge, try to cut",p," on each extreme"))
	    quant <- quantile(C,probs=c(p,1-p))
	    p <- 2*p
	    subset <- which(C>min(quant) & C<max(quant))
	    
	    # # old: add in if: && Nthin<length(C)/100
	    # Nthin<-Nthin*10
	    # subset<-seq(1,length(C),Nthin)
    }
    attributes(densfit)$Nthin<-Nthin
    if(Nthin>1){
      name<-paste(name," thinned x",Nthin,sep="")
    }

    vals<-predict(densfit,estimate)
    if(visu){
	    if(is.null(xlim)){
		    xlim = range(C)
	    }
	    x<-seq(xlim[1],xlim[2],length.out=1000)
	    y<-predict(densfit,x)
	    if(add){
	    lines(x,y,type="l",...)
	    }else{
	    plot(x,y,type="l",xlab=name,ylab="density", xlim = xlim,ylim=range(y),...)
	    }
	    if(visuQuant){
		    lines(rep(estimate[1],2),c(0,vals[1]),col="black")
		    for(q in 2:4){
			    lines(rep(estimate[q],2),c(0,vals[q]),col="blue")
		    }
		    if(!is.null(true.val)){
			    abline(v=true.val,col="green")
		    }
	    }

	    if(leg){ # legend
		    # legend
		    if(mean(densfit$box)>estimate[3]){
			    loc<-"topright"
		    }else{
			    loc<-"topleft"
		    }
		    leg.text<-c(paste("CrI/med.",signif(estimate[3],3)),paste("Mean",signif(estimate[1],3)))
		    leg.col<-c("blue","black")
		    if(!is.null(true.val)){
			    leg.text<-c(leg.text,paste("True val.(",true.val,")",sep=""))
			    leg.col<-c(leg.col,"green")
		    }
		    legend(loc,leg.text,col=leg.col,lty=1)
	    }
    }
  }else{
    densfit<-NULL
    vals<-NULL
  }
  attributes(estimate)$densfit<-densfit
  attributes(estimate)$vals<-vals

  return(estimate)
}

#' @title compute pdfs according of frequencies 
#' @description Compute smoothed density functions according to the 
#'         density of values in the columns of the table in entry.
#'         The density is estimated for each column separatly 
#'         and all are plotted together, it is based on \code{GetEstimate}()
#' @param db a table, coerced to a data frame with in columns each variable
#'        you want to estimate the pdf of.
#' @param nl the number of lines of plots in the plotting device
#' @param nc the number of columns of plots in the plotting device
#' @param true.vals values you want to signal on the plots with a vertical line
#' @param visu TRUE/FALSE, want to plot the density functions?
#' @return the output of \code{GetEstimate} for each variable
#' @export GetDensityFromValues
GetDensityFromValues <- function(db,nl=3,nc=4,true.vals=NULL,visu=TRUE){
	db<-as.data.frame(db)
	estimates<-list()
	for(num in 1:dim(db)[2]){
		if(num %% (nl*nc) ==1){ 
			dev.new()
			par(mfrow=c(nl,nc))
		}
		name<-names(db)[num]
		if(name %in% names(true.vals)){
			true.val<-true.vals[[name]]
		}else{
			true.val<-NULL
		}
		estimates[[name]]<-GetEstimate(db[,num],name=name,true.val=true.val,visu=visu)
	}
	return(estimates)
}

NULL
